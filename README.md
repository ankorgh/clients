# Clients

## Instructions

### Install docker  (if you don't have docker installed)
For easy development purposes docker was used. Install Docker based on system's operating system. 
You can install Docker following the instructions [here](https://docs.docker.com/get-docker/).

### Clone the repo
```bash
git clone https://ankorgh@bitbucket.org/ankorgh/clients.git
```

### Start the project
```bash
MONGO_URI="<path-to-mongo-db-connection>" docker-compose up --build

# for easy setup this external mongo setup can be used
MONGO_URI="mongodb+srv://dev:devpass@clientscluster.9uybt.mongodb.net/clients_db?retryWrites=true&w=majority" docker-compose up --build
```

### Stop the project
```bash
docker-compose down
```

### View
Client should be available on [localhost:3000](http://localhost:3000) while server should be listening on [localhost:9910](http://localhost:9910)

## Technologies
- Node (Express)
- Vue 
- Mongo 

