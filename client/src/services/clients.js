import { instance } from './api'

export const getClients = () => {
    return instance.get('/clients')
}

export const createClient = (payload) => {
    return instance.post('/clients', payload)
}

export const deleteClient = (id) => {
    return instance.delete(`/clients/${id}`)
}

export const editClient = (id, payload) => {
    return instance.put(`/clients/${id}`, payload)
}