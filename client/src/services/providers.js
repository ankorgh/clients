import { instance } from './api'

export const getProviders = () => {
    return instance.get('/providers')
}

export const createProvider = (payload) => {
    return instance.post('/providers', payload)
}

export const deleteProvider = (id) => {
    return instance.delete(`/providers/${id}`)
}

export const editProvider = (id, payload) => {
    return instance.put(`/providers/${id}`, payload)
}