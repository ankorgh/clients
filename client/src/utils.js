export const handleError = (error) => {
    let message = ""
    if (error.response) {
        const { data: { message: responseMessage, errors } } = error.response
        if(errors && Array.isArray(errors)) {
            errors.forEach((err) => {
                if (err.msg) {
                    message += err.msg + "\n"
                }
            })
        } else {
            message = responseMessage
        } 
    } else {
        message = 'Oops, failed to load. Don\'t fret, retry'
    }
    return message
}

export const isValidEmail = (email)  => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

