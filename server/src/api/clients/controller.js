const { Error } = require("mongoose")
const { StatusCodes, ReasonPhrases } = require("http-status-codes")

const { logger } = require("../../logger")
const { ClientModel } = require("./model")
const { SortDirection } = require("./types")

const getClients = async (req, res) => {
    let { email, name, sort_by: sortBy, sort_direction: sortDirection } = req.query   
    try {
        const filterOptions = {}
        const sortOptions  = {}
       
        if (sortBy && typeof sortBy === 'string') {
            sortBy = sortBy.toLowerCase()
            if(sortBy !== 'email' && sortBy !== 'name') {
                return res.status(StatusCodes.BAD_REQUEST).json({
                    message: `sort by 'name' or 'email'`
                })
            }
            if (!sortDirection) {
                sortDirection = 'asc'
            } else if (typeof sortDirection === 'string') {
                sortDirection = sortDirection.toLowerCase()
                if (sortDirection !== 'asc' && sortDirection !== 'desc') {
                    return res.status(StatusCodes.BAD_REQUEST).json({
                        message: `sort direction should be 'asc' or 'desc'`
                    })
                }
            }
        }

        if (name) {
            filterOptions.name = {
                $regex: new RegExp(`${name}`),
                $options: 'i'
            }
        }
        if (email) {
            filterOptions.email = {
                $regex: new RegExp(`${email}`),
                $options: 'i'
            }
        }
        if (sortBy && typeof sortBy === 'string' && sortDirection === 'asc') {
            sortOptions.sort = {
                [sortBy]: SortDirection.ASC
            }
        }
        if (sortBy  && typeof sortBy === 'string' && sortDirection === 'desc') {
            sortOptions.sort = {
                [sortBy]: SortDirection.DESC
            }
        }
        
        const clients = await ClientModel.find(filterOptions, {}, sortOptions)
        return res.json({
            message: "successfully retrieved all clients",
            data: clients
        })
    } catch (error) {
        logger.error(error)
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}

const deleteClient = async (req, res) => {
    const { id } = req.params
    try {
        const deleteResult = await ClientModel.deleteOne({
            _id: id,
        })
        if (deleteResult.deletedCount >= 1) {
            return res.sendStatus(StatusCodes.NO_CONTENT)
        }
        return res.status(StatusCodes.NOT_FOUND).json({
            message: `client id '${id}' not found`
        })
    } catch(error) {
        logger.error(error)
        if (error instanceof Error.CastError) {
            return res.status(StatusCodes.BAD_REQUEST).json({
                message: `invalid client id '${id}'`
            })
        }
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}

const updateClient = async (req, res) => {
    const { id } = req.params
    const { name, email, phone, providers } = req.body
    try {
        const client = await ClientModel.findById(id)
        if (!client) {
            return res.status(StatusCodes.NOT_FOUND).json({
                message: ReasonPhrases.NOT_FOUND
            })
        }
        
        client.name = name || client.name
        client.email = email || client.email
        client.phone = phone || client.phone
        client.providers = providers || client.providers

        await client.save()
        return res.json({
            message: `update client with '${id}' successfully`,
            data: client
        })
    } catch (error) {
        logger.error(error)
        if (error instanceof Error.CastError) {
            return res.status(StatusCodes.BAD_REQUEST).json({
                message: `invalid client id '${id}'`
            })
        }
        if (error.code === 11000) {
            const duplicateKeys = Object.keys(error.keyValue)
            if (duplicateKeys.length >= 1) {
                return res.status(StatusCodes.CONFLICT).json({
                    message: `client with ${duplicateKeys[0]} '${error.keyValue[duplicateKeys[0]]}' already created`
                })
            }
        }
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}


const createClient = async (req, res) => {
    const { name, email, phone, providers } = req.body
    try {
        const client = new ClientModel()
        client.name = name
        client.email = email
        client.phone = phone
        client.providers = providers

        await client.save()
        return res.status(StatusCodes.CREATED).json({
            message: 'new client created successfully',
            data: client,
        })
    } catch(error) {
        logger.error(error)
        if (error.code === 11000) {
            const duplicateKeys = Object.keys(error.keyValue)
            if (duplicateKeys.length >= 1) {
                return res.status(StatusCodes.CONFLICT).json({
                    message: `client with ${duplicateKeys[0]} '${error.keyValue[duplicateKeys[0]]}' already created`
                })
            }
        }
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}

module.exports = {
    getClients,
    deleteClient,
    createClient,
    updateClient
}