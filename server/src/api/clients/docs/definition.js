const clientDefinition = {
        type: 'Object',
        properties: {
            _id: {
                type: 'string'
            },
            name: {
                type: 'string'
            },
            email: {
                type: 'string'
            },
            phone: {
                type: 'string'
            },
            providers: {
                type: 'array'
            },
            createdAt: {
                type: 'string'
            },
            updatedAt: {
                type: 'string'
            }
        },
        example: {
            _id: "",
            name: "",
            email: "",
            phone: "",
            providers: [],
            createdAt: "",
            updatedAt: ""
        }
}

const clientRequestBody =  {
    type: 'Object',
    properties: {
        name: {
            type: 'string'
        },
        email: {
            type: 'string'
        },
        phone: {
            type: 'string'
        },
        providers: {
            type: 'array'
        }
    },
    example: {
        name: "",
        email: "",
        phone: "",
        providers: []
    }
}

module.exports = {
    clientRequestBody,
    clientDefinition
}