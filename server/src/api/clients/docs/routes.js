const { clientTag } = require("./tag")

const clientPath = {
    // get all clients
    get: {
        tags: [
            clientTag
        ],
        summary: 'Get all clients',
        responses: {
            200: {
                description: 'OK',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                                data: {
                                    "$ref": "#/components/schemas/Client"
                                }
                            },
                            example: {
                                message: "OK",
                                data: []
                            }
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Server Error"
                            }
                        }
                    }
                }
            }
        }
    },

    // create a client
    post: {
        tags: [
            clientTag
        ],
        summary: 'Create a client',
        requestBody: {
            description: 'client request body',
            required: true,
            content: {
                'application/json': {
                    schema: {
                        "$ref": "#/components/schemas/ClientRequestBody"
                    }
                }
            }
        },
        responses: {
            201: {
                description: 'Created',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                                data: {
                                    "$ref": "#/components/schemas/Client"
                                }
                            },
                            example: {
                                message: "Created"
                            }
                        }
                    }
                }
            },
            409: {
                description: 'Conflict',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Conflict"
                            }
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Server Error"
                            }
                        }
                    }
                }
            }
        }
    },
}

const singleClientPath = {
    // parameters
    parameters: [
        {
            name: 'id',
            in: 'path',
            required: true,
            description: 'client id',
            schema: {
                type: 'string'
            }
        }
    ],

    // update a client
    put: {
        tags: [
            clientTag
        ],
        summary: 'Update a single client',
        responses: {
            200: {
                description: 'OK',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                                data: {
                                    "$ref": "#/components/schemas/Client"
                                }
                            },
                            example: {
                                message: "OK",
                                data: {}
                            }
                        }
                    }
                }
            },
            400: {
                description: 'Bad Request',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Bad Request"
                            }
                        }
                    }
                }
            },
            409: {
                description: 'Conflict',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Conflict"
                            }
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Server Error"
                            }
                        }
                    }
                }
            }
        }
    },

    // delete a client
    delete: {
        tags: [
            clientTag
        ],
        summary: 'Delete a single client',
        responses: {
            200: {
                description: 'OK',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                                data: {
                                    "$ref": "#/components/schemas/Client"
                                }
                            },
                            example: {
                                message: "OK",
                                data: {}
                            }
                        }
                    }
                }
            },
            400: {
                description: 'Bad Request',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Bad Request"
                            }
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Server Error"
                            }
                        }
                    }
                }
            }
        }
    }
}

module.exports = {
    clientPath,
    singleClientPath
}