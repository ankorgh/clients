const { Types } = require("mongoose")
const { body } = require("express-validator")
const { isValidPhoneNumber } = require("libphonenumber-js")

const CreateClientValidations = [
    body("name")
        .isString()
        .withMessage("invalid type. name should be a string")
        .notEmpty()
        .withMessage("name is required")
        .isLength({
            min: 1
        })
        .withMessage("name should be atleast a single letter"),
    
    body("email")
        .isString()
        .withMessage("invalid type. email should be a string")
        .notEmpty()
        .withMessage("email is required")
        .isEmail()
        .withMessage("email is not valid"),
    
    body("phone")
        .isString()
        .withMessage("invalid type. phone should be a string")
        .notEmpty()
        .withMessage("phone number is required")
        .custom((value) => {
            return isValidPhoneNumber(value)
        })
        .withMessage("invalid phone number"),
    
    body("providers")
        .isArray({
            min: 1
        })
        .withMessage("should contain atleast a single provider")
        .custom((providers) => {
            return Array.isArray(providers) && providers.reduce((isObjectId, provider) => {
                try {
                    new Types.ObjectId(provider)
                    return isObjectId && true
                } catch(error) {
                    return isObjectId && false
                }
            }, true);

        })
        .withMessage("provider ids should be valid object ids")
]

module.exports = {
    CreateClientValidations
}