const { Router } = require("express")

const { validate } = require("../../middlewares/validate")
const { CreateClientValidations } = require("./dtos/create-client.dto")
const { UpdateClientValidations } = require("./dtos/update-client.dto")
const { createClient, deleteClient, getClients, updateClient } = require("./controller")

const router = Router()

router.get("/", getClients)
router.post("/", validate(CreateClientValidations), createClient)
router.put("/:id", validate(UpdateClientValidations), updateClient)
router.delete("/:id", deleteClient)

module.exports = router