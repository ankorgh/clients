// import { Schema, model } from "mongoose"
const { Schema, model } = require("mongoose")

const ClientSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    phone: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    providers: [
        {
            type: 'ObjectId',
            ref: 'Provider'
        }
    ]
}, {
    timestamps: true
})

const ClientModel = model('Client', ClientSchema)

module.exports = {
    ClientModel
}