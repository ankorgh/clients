const SortDirection = {
    ASC: 1,
    DESC: -1
}

module.exports = {
    SortDirection
}