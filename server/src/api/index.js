const { Router  } = require("express")
const swaggerUi = require("swagger-ui-express")

const { swaggerDocs } = require("./swagger-docs")
const clientRouter = require("./clients")
const providerRouter = require("./providers")

const router = Router()

router.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs))
router.use("/clients", clientRouter)
router.use("/providers", providerRouter)

module.exports = router