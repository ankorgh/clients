const { Error } = require("mongoose")
const { ReasonPhrases, StatusCodes } = require("http-status-codes")

const { logger } = require("../../logger")
const { ProviderModel } = require("./model")

const createProvider = async (req, res) => {
    const { name } = req.body
    try {
        const provider = new ProviderModel()
        provider.name = name
        await provider.save()
        return res.status(StatusCodes.CREATED).json({
            message: "successfully created provider",
            data: provider
        })
    } catch (error) {
        logger.error(error)
        if (error.code === 11000) {
            return res.status(StatusCodes.CONFLICT).json({
                message: `provider with name '${name}' already created`
            })
        }
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}

const getProviders = async (req, res) => {
    try {
        const providers = await ProviderModel.find()
        return res.json({
            message: "successfully retrieved all providers",
            data: providers
        })
    } catch (error) {
        logger.error(error)
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}

const deleteProvider = async (req , res) => {
    const { id } = req.params
    try {
        const provider = await ProviderModel.findOne({ _id: id })
        if(!provider) {
            return res.status(StatusCodes.NOT_FOUND).json({
                message: `provider id '${id}' not found`
            })
        }
        
        await provider.delete()
        return res.sendStatus(StatusCodes.NO_CONTENT)
    } catch(error) {
        logger.error(error)
        if (error instanceof Error.CastError) {
            return res.status(StatusCodes.BAD_REQUEST).json({
                message: `invalid provider id '${id}'`
            })
        }
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}

const updateProvider = async (req, res) => {
    const { id } = req.params
    const { name }  = req.body
    try {
        const provider = await ProviderModel.findById(id)
        if (!provider) {
            return res.status(StatusCodes.NOT_FOUND).json({
                message:  `provider id '${id}' not found`
            })
        }

        provider.name = name || provider?.name
        await provider.save()

        return res.json({
            message: `provider with id '${id}' updated successfully`,
            data: provider
        })
    } catch(error) {
        logger.error(error)
        if (error instanceof Error.CastError) {
            return res.status(StatusCodes.BAD_REQUEST).json({
                message: `invalid provider id '${id}'`
            })
        }
        if (error.code === 11000) {
            return res.status(StatusCodes.CONFLICT).json({
                message: `provider with name '${name}' already created`
            })
        }
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({
            message: ReasonPhrases.INTERNAL_SERVER_ERROR
        })
    }
}

module.exports = {
    createProvider,
    getProviders,
    deleteProvider,
    updateProvider
}