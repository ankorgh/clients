const providerDefinition = {
        type: 'Object',
        properties: {
            _id: {
                type: 'string'
            },
            name: {
                type: 'string'
            },
            createdAt: {
                type: 'string'
            },
            updatedAt: {
                type: 'string'
            }
        },
        example: {
            _id: "",
            name: "",
            createdAt: "",
            updatedAt: ""
        }
}

const providerRequestBody = {
    type: 'Object',
    properties: {
        name: {
            type: 'string'
        },
    },
    example: {
        name: ""
    }
}

module.exports = {
    providerDefinition,
    providerRequestBody
}