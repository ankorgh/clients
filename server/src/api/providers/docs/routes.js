const { providerTag } = require("./tag")

const providerPath = {
    // get all providers
    get: {
        tags: [
            providerTag
        ],
        summary: 'Get all providers',
        responses: {
            200: {
                description: 'OK',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                                data: {
                                    type: 'array',
                                    items: {
                                        '$ref': '#/components/schemas/Provider'
                                    }
                                }
                            }
                        },
                        example: {
                            message: "OK",
                            data: []
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Server Error"
                            }
                        }
                    }
                }
            }
        }
    },

    // create a provider
    post: {
        tags: [
            providerTag
        ],
        summary: 'Create a provider',
        requestBody: {
            description: 'provider request body',
            required: true,
            content: {
                'application/json': {
                    schema: {
                        "$ref": "#/components/schemas/ProviderRequestBody"
                    }
                }
            }
        },
        responses: {
            201: {
                description: 'Created',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                                data: {
                                    '$ref': '#/components/schemas/Provider'
                                }
                            },
                            example: {
                                message: "Created",
                                data: {}
                            }
                        }
                    }
                }
            },
            409: {
                description: 'Conflict',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                            },
                            example: {
                                message: "Conflict"
                            }
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Server Error"
                            }
                        }
                    }
                }
            }
        }
    },
}

const singleProviderPath = {
    // parameters
    parameters: [
        {
            name: 'id',
            in: 'path',
            required: true,
            description: 'provider id',
            schema: {
                type: 'string'
            }
        }
    ],

    // update a provider
    put: {
        tags: [
            providerTag
        ],
        summary: 'Get all providers',
        responses: {
            200: {
                description: 'Ok',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                                data: {
                                    "$ref": "#/components/schemas/Provider"
                                }
                            },
                            example: {
                                message: "",
                                data: {}
                            }
                        }
                    }
                }
            },
            409: {
                description: 'Conflict',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                            },
                            example: {
                                message: "Conflict"
                            }
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Server Error"
                            }
                        }
                    }
                }
            }
        }
    },

    // delete a provider
    delete: {
        tags: [
            providerTag
        ],
        summary: 'Delete a single provider',
        responses: {
            204: {
                description: 'No content',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                            },
                            example: {
                                message: "No content"
                            }
                        }
                    }
                }
            },
            400: {
                description: 'Bad Request',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                },
                            },
                            example: {
                                message: "Bad Request"
                            }
                        }
                    }
                }
            },
            500: {
                description: 'Internal Server Error',
                content: {
                    'application/json': {
                        schema: {
                            type: 'Object',
                            properties: {
                                message: {
                                    type: 'string'
                                }
                            },
                            example: {
                                message: "Internal Serrver Error"
                            }
                        }
                    }
                }
            }
        }
    }
}

module.exports = {
    providerPath,
    singleProviderPath
}