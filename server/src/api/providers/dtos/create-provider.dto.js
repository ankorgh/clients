const { body } = require("express-validator")

// import { IsNotEmpty, IsString, MinLength } from "class-validator";

// export class CreateProviderDto {
//     @IsString({
//         message: 'invalid name type. name should be a sequence of letters'
//     })
//     @MinLength(1, {
//         message: 'name should be atleast a single letter'
//     })
//     @IsNotEmpty({
//         message: "name is required"
//     })
//     name!: string;
// }

const CreateProviderValidations = [
    body("name")
        .isString()
        .withMessage("invalid type. name should be a string")
        .notEmpty()
        .withMessage("name is required")
        .isLength({
            min: 1
        })
        .withMessage("name should be atleast a single letter")
]

module.exports = {
    CreateProviderValidations
}