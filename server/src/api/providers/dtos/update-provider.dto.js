const { body } = require("express-validator")

// import { IsString, MinLength, IsOptional } from "class-validator";

// export class UpdateProviderDto {
//     @IsOptional()
//     @IsString({
//         message: 'invalid name type. name should be a sequence of letters'
//     })
//     @MinLength(1, {
//         message: 'name should be atleast a single letter'
//     })
//     name?: string;
// }

const UpdateProviderValidations = [
    body("name")
        .isString()
        .withMessage("invalid type. name should be a string")
        .isLength({
            min: 1
        })
        .withMessage("name should be atleast a single letter")
]

module.exports = {
    UpdateProviderValidations
}