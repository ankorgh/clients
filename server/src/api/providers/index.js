const { Router } = require("express")

const { validate } = require("../../middlewares/validate")
const { CreateProviderValidations } = require("./dtos/create-provider.dto")
const { UpdateProviderValidations } = require("./dtos/update-provider.dto")
const { createProvider, deleteProvider, getProviders, updateProvider } = require("./controller")

const router = Router()

router.get("/", getProviders)
router.post("/", validate(CreateProviderValidations), createProvider)
router.put("/:id", validate(UpdateProviderValidations), updateProvider)
router.delete("/:id", deleteProvider)

module.exports = router