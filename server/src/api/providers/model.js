const { Schema, model } = require("mongoose")

const { logger } = require("../../logger")
const { ClientModel } = require("../clients/model")

const ProviderSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true
    }
}, {
    timestamps: true
})

ProviderSchema.post("remove", async document => {
    const providerId = document._id
    try {
        await ClientModel.updateMany({ providers: { $in: [providerId] } }, { $pull : { providers: providerId } }, { new: true })
    } catch(error) {
        logger.error(error)
    }
})

const ProviderModel = model('Provider', ProviderSchema)

module.exports = {
    ProviderModel
}