const { Config }  = require("../config")
const { clientTag }  = require("./clients/docs/tag")
const { providerTag } = require("./providers/docs/tag")
const { clientDefinition, clientRequestBody } = require("./clients/docs/definition")
const { providerDefinition, providerRequestBody } = require("./providers/docs/definition")
const { clientPath, singleClientPath } = require("./clients/docs/routes")
const { providerPath, singleProviderPath } = require("./providers/docs/routes")

const swaggerDocs = {
    openapi: "3.0.3",
    info: {
      title: "Clients REST API", 
      version: "1.0.0",
      contact: {
        name: "Abdul-Harisu Inusah",
      },
    },
    consumes: ["application/json"],
    produces: ["application/json"],
    servers: [
        {
            url: `http://localhost:${Config.PORT}/api/v1/`,
            description: "Local Server",
        }
    ],
    tags: [
        providerTag,
        clientTag
    ],
    paths: {
      '/providers': providerPath,
      '/providers/{id}': singleProviderPath,
      '/clients': clientPath,
      '/clients/{id}': singleClientPath
    },
    components: {
      schemas: {
        'Provider': providerDefinition,
        'ProviderRequestBody': providerRequestBody,
        'Client': clientDefinition,
        'ClientRequestBody': clientRequestBody
      }
    }
}

module.exports = {
  swaggerDocs
}