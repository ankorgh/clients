const cors = require("cors")
const { StatusCodes, ReasonPhrases } = require("http-status-codes")
const express = require("express")

const apiRouter = require("./api")

const app = express();

const notFoundErrorHandler = (req, res, next) => {
    return res.status(StatusCodes.NOT_FOUND).json({
        message: ReasonPhrases.NOT_FOUND
    })
}

app.use(cors())
app.use(express.json())

app.use("/api/v1", apiRouter)

app.use(notFoundErrorHandler)

module.exports = app 
