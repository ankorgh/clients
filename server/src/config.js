const dotenv = require("dotenv")

dotenv.config()

const Config = {
    PORT: process.env.PORT || 9898,
    MONGO_URI: process.env.MONGO_URI || ""
}

module.exports = {
    Config
}