const mongoose = require("mongoose")

const { Config }  = require("./config")
const { logger } = require("./logger")

class MongoConnection {
    _client

    get connection() {
        if (this._client) {
            logger.error("connection mongo database not established")
            return null
        }
        return this._client
    }

    connect() {
        return new Promise(async (resolve, reject) => {
            try {
                const connection = await mongoose.connect(Config.MONGO_URI, {})
                logger.info("Successfull connection to mongo database")
                this._client = connection
                resolve(connection)
            } catch (error) {
                logger.error(error)
                reject(error)
            }
        })
    }
}

module.exports = new MongoConnection()