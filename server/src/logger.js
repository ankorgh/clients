const pino = require("pino")

const prettyTransport = pino.transport({
    target: "pino-pretty",
    options: {
        destination: 1
    }
})

const logger = pino(prettyTransport)

module.exports = {
    logger
}