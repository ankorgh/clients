const { ReasonPhrases, StatusCodes } = require("http-status-codes")
const { validationResult } = require("express-validator")

const { logger } = require("../logger")

const validate = validations => async (req, res, next) => {
  try {
    await Promise.all(validations.map(validation => validation.run(req)));
    const errors = validationResult(req);

    if (errors.isEmpty()) {
      return next();
    }

    res.status(StatusCodes.BAD_REQUEST).json({ 
      message: ReasonPhrases.BAD_REQUEST,
      errors: errors.array() 
    });
  } 
  catch(err) {
    logger.error(error)
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR)
      .json({ 
        message: ReasonPhrases.INTERNAL_SERVER_ERROR 
      })
  }
};
  

module.exports = {
    validate
}