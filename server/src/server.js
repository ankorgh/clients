const app = require("./app")
const { Config } = require("./config")
const { logger } = require("./logger")
const mongoConnection = require("./db")

const setup = async () => {
    try {
        await mongoConnection.connect()
        app.listen(Config.PORT, () => {
            logger.info(`Listening on port: ${Config.PORT}`)
        })
    } catch (error) {
        logger.error(error)
        logger.error('failed to start app')
        process.exit(1)
    }
}

setup()